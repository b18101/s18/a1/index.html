
	
/*	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function*/

		console.log("Displayed sum of 2 and 1");

		function addTwoNum(num1,num2){
			console.log(num1 + num2);
		};

		addTwoNum(2,1);

		console.log("Displayed difference of 2 and 1");

		function subtractTwoNum (num1,num2){
			console.log(num1 - num2);
		};

		subtractTwoNum(2,1);

/*2.  	Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication. (use return statement)

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division. (use return statement)

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console. */

		console.log("The product of 2 and 2");

		function multiplyTwoNum(num1, num2){
			return num1 * num2
		};

		let product = multiplyTwoNum(2,2);
		console.log(product);

		console.log("The quotient of 2 and 2");

		function divideTwoNum(num1, num2){
			return num1 / num2
		};

		let quotient = divideTwoNum(2,2);
		console.log(quotient);

/*	3. 	Create a function which will be able to get total area of a circle from a provided radius.
			-a number should be provided as an argument.
			-formula: area = pi * (radius ** two)
			-exponent operator **
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation. (use return statement)

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

		Log the value of the circleArea variable in the console. */

		console.log("The result of getting the area of a circle with 20 radius");

		function totalArea(pi,radius){
			return pi * (radius ** 2);
		};

		let circleArea = totalArea(3.14, 20);
		console.log(circleArea);

/*	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-formula for average: num1 + num2 + num3 + ... divide by total numbers
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation. (use return statement) 

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.*/

		console.log("The average of 2,4,6,8");

		function averageFourNum(num1, num2, num3, num4){
			return (num1 + num2 + num3 + num4) / 4;
		};

		let averageVar = averageFourNum(2,4,6,8);
		console.log(averageVar);
	

/*	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed. (use return statement)
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

	console.log("Is 75/75 a passing score?");

	function checkScore(myScore, passingScore){
		let compute = (myScore / passingScore) * 100;
		let isPassed = compute > passingScore;
		return isPassed;

	};

	let isPassingScore = checkScore(75,75);
	console.log(isPassingScore);
